import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {

  }
      public getSystem() {
        const url = 'https://www.mocky.io/v2/5c6420a8320000791c93f805';
        return this.http.get(url, {withCredentials: false, responseType: 'json'});
      }

      public getUser() {
        const url = 'https://www.mocky.io/v2/5c6420c8320000f01993f807';
        return this.http.get(url, {withCredentials: false, responseType: 'json'});
      }

      public getHora(): Observable<string> {
        const url = 'https://www.mocky.io/v2/5c642040320000fe1293f801';
        return this.http.get<string>(url, {withCredentials: false, responseType: 'json'});
      }

      public getModulos() {
        const url = 'https://www.mocky.io/v2/5c641fc5320000912f93f7fe';
        return this.http.get(url, {withCredentials: false, responseType: 'json'});
      }
}
