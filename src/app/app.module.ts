import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { FooterModule } from 'footer';
import { HeaderModule } from 'lib-header';
import { MenuModule } from 'lib-menu';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.modules';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FooterModule,
    HeaderModule,
    HttpClientModule,
    MenuModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
