export interface UsuarioLogadoInteface {
  id: number;
  username: string;
  nome: string;
  cpf: string;
  email: string;
  ddd: string;
  telefone: string;
}
