import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PesquisaForm } from './pesquisa.form';
import { TextMaskFactory } from 'src/app/util/text-mask-factory';

@Component({
  selector: 'app-processos-oficios-form',
  templateUrl: './processos-oficios.form.component.html',
  styleUrls: ['./processos-oficios.form.css']
})
export class ProcessosOficiosFormComponent implements OnInit {

  @Output() public dataForm: EventEmitter<any>;

  private _pesquisaForm: PesquisaForm;

  private _maskFactory: TextMaskFactory;

  constructor() {
    this._pesquisaForm = new PesquisaForm();
    this._maskFactory = new TextMaskFactory();
  }

  ngOnInit() { }

  public get pesquisaForm(): PesquisaForm{
    return this._pesquisaForm;
  }

  public get maskFactory(): TextMaskFactory{
    return this._maskFactory;
  }
}
