import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ProcessosOficiosFormComponent } from './processos-oficios.form.component';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TextMaskModule
  ],
  declarations: [ ProcessosOficiosFormComponent ],
  exports: [ ProcessosOficiosFormComponent ]
})
export class FormModule { }
