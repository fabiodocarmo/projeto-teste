import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

export class PesquisaForm extends FormGroup {

  private _errorMessages = {
    required: 'O campo %s é obrigatório.',
  };

  constructor() {
    super({
        cnpj: new FormControl(null, Validators.required),
        numeroRegistro: new FormControl(''),
        nomeEmpresarial: new FormControl(''),
        cpfMembroQsa: new FormControl('')
    });
  }

  public get cnpj(): AbstractControl {
      return this.get('cnpj');
  }

  public get numeroRegistro(): AbstractControl {
    return this.get('numeroRegistro');
  }

  public get nomeEmpresarial(): AbstractControl {
    return this.get('nomeEmpresarial');
  }

  public get cpfMembroQsa(): AbstractControl {
    return this.get('cpfMembroQsa');
  }

  public getFirstErrorFrom(controlName: string, label: string): string {
    return this._errorMessages[Object.keys(this.get(controlName).errors)[0]].replace('%s', label || controlName);
  }

}
