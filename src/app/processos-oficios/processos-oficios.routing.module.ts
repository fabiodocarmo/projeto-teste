import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProcessosOficiosComponent } from './processos-oficios.component';

const routes: Routes = [
  {
    path: '',
    component: ProcessosOficiosComponent
  }, {
    path: 'visualizar',
    loadChildren: './visualizar/visualizar.module#VisualizarModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessosOficioRoutingModule { }
