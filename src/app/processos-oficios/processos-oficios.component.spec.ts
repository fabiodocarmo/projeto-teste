import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessosOficiosComponent } from "./ProcessosOficiosComponent";

describe('ProcessosOficiosComponent', () => {
  let component: ProcessosOficiosComponent;
  let fixture: ComponentFixture<ProcessosOficiosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessosOficiosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessosOficiosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
