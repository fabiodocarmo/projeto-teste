import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { GridSearchParams, GridColumnDefs } from 'grid';

@Component({
  selector: 'app-processos-oficios',
  templateUrl: './processos-oficios.component.html',
  styleUrls: ['./processos-oficios.component.css']
})
export class ProcessosOficiosComponent implements OnInit {

  private _colunasGrid: Array<GridColumnDefs>;

  constructor( private router: Router ) { }

  ngOnInit() {
  }

  public rota(): void {
    this.router.navigate(['processos-oficios/visualizar', 10]);
  }

  public get colunasGrid(): Array<GridColumnDefs>{
    return this._colunasGrid = [
      new GridColumnDefs('Nome Empresa', 'ds_nome_empresa', 250),
      new GridColumnDefs('CNPJ', 'ds_cnpj', 115),
      new GridColumnDefs('Número do Registro', 'ds_numero_registro', 130),
      new GridColumnDefs('Endereço', 'ds_endereco', 225),
    ];
  }


}
