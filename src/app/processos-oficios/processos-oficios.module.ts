import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProcessosOficiosComponent } from './processos-oficios.component';
import { ProcessosOficioRoutingModule } from './processos-oficios.routing.module';
import { FormModule } from './form/form.module';


@NgModule({
  declarations: [ ProcessosOficiosComponent ],
  imports: [ CommonModule, ProcessosOficioRoutingModule, FormModule ]
})
export class ProcessosOficiosModule { }
