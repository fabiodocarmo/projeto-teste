import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisualizarRoutingModule } from './visualizar-routing.module';
import { VisualizarComponent } from './visualizar.component';

@NgModule({
  imports: [
    CommonModule,
    VisualizarRoutingModule
  ],
  declarations: [VisualizarComponent],
  exports: [VisualizarComponent]
})
export class VisualizarModule { }
