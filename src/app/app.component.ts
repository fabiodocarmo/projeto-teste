import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from './services/user-service';
import { LogoInterface } from 'lib-header';
import { Subscription, forkJoin } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'initial';

  private _urlLogoSistema: LogoInterface;

  private _urlLogoVox: LogoInterface;

  private _sistema: any;

  private _usuario: any;

  private _hora: any;

  private _modulos: any;

  constructor(private userService: UserService) {

    this._urlLogoSistema = { url: 'assets/images/sigfacil.png', alt: 'Logo' };

    this._urlLogoVox = { url: 'assets/images/logo-vox.jpg', alt: 'Logo Sistema' };
  }

  public ngOnInit(): void {
    this.getSystemInfo();
  }

  public ngOnDestroy(): void {
    this.getSystemInfo().unsubscribe();
  }

  private getSystemInfo(): Subscription {
    return forkJoin(
      this.userService.getSystem(),
      this.userService.getUser(),
      this.userService.getHora(),
      this.userService.getModulos(),
    ).subscribe(([system, user, hora, modulos]) => {
      console.log(modulos);
        this._sistema = system;
        this._usuario = user;
        this._hora = hora;
        this._modulos = modulos;

    },
    (error: Error) => {
        console.log(error);
    });
  }

  public get urlLogoSistema(): LogoInterface {
    return this._urlLogoSistema;
  }

  public get urlLogoVox(): LogoInterface {
    return this._urlLogoVox;
  }

  public get sistema() {
    return this._sistema;
  }

  public get usuario() {
    return this._usuario;
  }

  public get dataSistema() {
    return this._hora;
  }

  public get itensMenu() {
    return this._modulos;
  }

  public getFuncionalidadeAtual() {

  }
}
